import React from "react";
import { View,Text, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList, TextInput } from "react-native";
import { LinearGradient } from 'expo-linear-gradient';

////IMAGENES//////
import ImgBack from '../../../assets/Calendar/goBack.svg'
import ImgPostDetail from '../../../assets/Calendar/postDetail.svg'
import ImgLocationDetail from '../../../assets/Calendar/locationDetail.svg'
import ListaPeople from "./listUsuarios";

const DetailEvets = ({navigation}) => {
  return ( 
        <View style={styles.container}>
            <LinearGradient
              colors={['#00B5C3', '#077477']}
              style={styles.portadaPost}>
                <View style={styles.header}>
                    <ImgBack></ImgBack>
                    <View style={styles.buttonNotificacion}>
                      <Text style={styles.textNotification}>Business After 5</Text>
                    </View>
                </View>
                <View style={[styles.flex, {
                    flexDirection: "row", justifyContent: 'space-between', marginTop: 30
                  }]}>
                  <View style={{flex: 1, marginLeft: 20, marginRight: 2}}>
                    <View style={styles.cuadroText}>
                      <Text style={styles.titleAnuncio}>Reindeer Run! 5K</Text>
                    </View>
                    <View style={styles.dateTime}>
                      <View style={styles.backgroundDate}>
                        <View style={styles.dateAll}>
                          <Text style={styles.nameDay}>Mon</Text>
                          <Text style={styles.dayDate}>19</Text>
                          <Text style={styles.nameMonth}>Dec</Text>
                        </View>
                      </View>
                      <View style={styles.detailDate}>
                        <Text style={styles.from}>From</Text>
                        <Text style={styles.dateStart}>6:00 PM</Text>
                        <Text style={styles.to}>To</Text>
                        <Text style={styles.dateEnd}>10:00 PM</Text>
                      </View>
                    </View>
                    <View style={styles.detailLocation}>
                      <View style={{flexDirection: 'row'}}>
                        <ImgLocationDetail/>
                        <Text style={styles.descriptionLocation}>300 Tower RoadNaples, FL 34113</Text>
                      </View>
                    </View>
                  </View>
                  
                  <View style={{ flex: 1, marginRight: 20,marginLeft: 2}} >
                    <ImgPostDetail style={styles.imgPost}/>
                  </View>
                </View>
            </LinearGradient>
            <View  style={[styles.flex, {
                    flexDirection: "row", marginTop: 20, marginRight: 20,marginLeft: 20,
                  }]}>
              <View style={{ flex: 4, height: 30}} >
                <View style={{flexDirection: 'row',}}>
                  <ListaPeople/>
                  <Text style={styles.peopleActivos}>Over 40 people joined this event</Text>
                </View>
              </View>
              <View style={{ flex: 1, marginRight: 20,marginLeft: 20, height: 30}} >
                {/* <ListaPeople/> */}
                <Text style={styles.monto}>450$</Text>
              </View>

            </View>
            <View  style={[styles.flex, {
                    flexDirection: "column", marginTop: 20, marginRight: 20,marginLeft: 20,
                  }]}>
              <View style={{ flex: 1,}} >
                <Text style={styles.descriptionEvent}>
                  Anyone who has been to a Girls on the Run 5K knows that it is unlike any other running event. It is a celebration of and for the girls who are completing an 10 week season of Girls on the Run programming, coaches, and the community! This y... View more
                </Text>
                
              </View>
            </View>
            

        </View>
  )
    
}

export default DetailEvets;

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    paddingTop: 32,
  },
  portadaPost: {
    width: '100%',
    height: 320,
    borderBottomRightRadius: 17,
    borderBottomLeftRadius: 17,
    //backgroundColor: '#00B5C3, #077477'
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 15,
    paddingLeft: 20,
    paddingRight:20
 
  },
  buttonNotificacion: {
    width: 105,
    height: 20,
    backgroundColor: '#FDB827',
    borderRadius: 10
  },
  textNotification:{
    textAlign: 'center',
    color: '#fff',
    alignSelf: 'center'
  },
  flex: {
    flex: 1,
  },
  cuadroText: {
    width: 170,
    height: 90,
    //backgroundColor: 'blue',
  },

  titleAnuncio: {
    fontSize: 30,
    fontWeight: '700',
    color: '#fff'

  },
  dateTime:{
    flexDirection: 'row',
    //backgroundColor: 'green',
    width: 170,
    height: 90,
  },

  backgroundDate:{
    width: 65,
    height:65,
    backgroundColor: '#fff',
    borderRadius: 9,
    alignSelf: 'center'
  },
  dateAll:{
    textAlign: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    padding: 6
  },
  nameDay: {
    color: '#FF0000',
    fontWeight: '400',
    fontSize: 12,
  },
  dayDate: {
    color: '#000',
    fontWeight: '700',
    fontSize: 24,
  },
  nameMonth: {
    color: '#000',
    fontWeight: '400',
    fontSize: 12,
  },
  detailDate: {
    marginLeft: 10,
    alignSelf: 'center'
  },
  from: {
    fontSize: 7,
    color: '#fff',
    fontWeight: '300'

  },
  dateStart:{
    fontSize: 20,
    fontWeight:'700',
    color: '#fff'
  },
  to: {
    fontSize: 7,
    color: '#fff',
    fontWeight: '300'
  },
  dateEnd:{
    fontSize: 20,
    fontWeight:'700',
    color: '#fff'
  },
  detailLocation: {
    width: 170,
    height: 50,
    //backgroundColor: 'red',
    
  },
  descriptionLocation: {
    fontSize: 15,
    fontWeight:'400',
    color: '#fff',
    marginLeft: 10,
    
  },

  peopleActivos: {
    fontSize: 12,
    fontWeight: '600',
    color: '#000',
    marginLeft: 5,
    alignSelf: 'center'
  },
  monto: {
    fontSize: 16,
    fontWeight: "300",
    color: '#00B5C3',
    textAlign: 'right'
  },
  descriptionEvent: {
    fontSize: 12,
    fontWeight: '300',
    color: '#747474'
  }


 
});
