import React from "react";
import { View,Text, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList, TextInput } from "react-native";


//////////IMAGENES///////////
import ImgPostEvet from '../../assets/Calendar/postEvent.svg'
const PostEvents = () => {

    return(
        <View>
            <View style={styles.postSeccion}>
              <View>
                  <Text style={styles.title}>
                    Past Events
                  </Text>
              </View>
              <View >
                  <Text style= {styles.View}>
                      View all
                  </Text>
              </View>
            </View>
            <View>
                <View>
                    <ImgPostEvet/>
                </View>
                <View style={styles.seccionText}>
                    <Text style={styles.titleCard}>
                        Greater Naples Chamber Jingle & Mingle
                    </Text>
                    <Text style={styles.descriptionCard}>
                        Networking Event
                    </Text>
                </View>

            </View>

        </View>
    )
}

export default PostEvents;

const styles = StyleSheet.create ({
    postSeccion: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 30,
      },
    title: {
    color: '#000000',
    fontSize: 17,
    fontWeight: 'bold'
    },
    view: {
    color: '#747474',
    fontSize: 14,
    fontWeight: '300'
    },
    seccionText:{
    marginLeft: 15,
    marginRight:15
    },
    titleCard: {
    fontSize: 14,
    fontWeight: '700',
    marginTop: 15
    },
    descriptionCard: {
    fontSize: 14,
    fontWeight: '300',
    marginTop: 10
    },

})