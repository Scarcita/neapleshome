import React from "react";
import { View,Text, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList, TextInput } from "react-native";


////IMAGENES//////

import ImgLocation from '../../assets/Calendar/location.svg'
import FeatureEvents from "../CalendarEvents/featuredEvents";
import UpcomingEvents from "../CalendarEvents/upcomingEvents";
import PostEvents from "../CalendarEvents/postEvents";

const CalendarEvents = ({navigation}) => {


  return ( 
        <View style={styles.container}>
          <Text style={styles.location}>
            Current location
          </Text>
          <View style= {styles.header}>
            <View style= {styles.headerLeft}>
                <ImgLocation
                style={styles.ImgLocation}
                />
                <Text style={styles.locationName}>
                    Naples FL
                </Text>
            </View>
            <View >
                <Text style= {styles.headerRigh}></Text>

            </View>

          </View>

          <View>
            <TextInput 
            style={styles.input}
            placeholder='Search event'
            ></TextInput>
          </View>
          <View>
            <FeatureEvents/>
          </View>
          <View>
            <UpcomingEvents/>
          </View>
          <View>
            <PostEvents/>
          </View>
        </View>
      
    
  )
    
}

export default CalendarEvents;

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    marginTop: 30,
    marginLeft: 35,
    marginRight:35
    
    
  },
  location: {
    color: '#00B5C3',
    fontStyle: 'normal',
    fontSize: 13,
    marginTop: 35,

  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  headerLeft: {
    flexDirection: 'row',
  },
  locationName: {
    color: '#00B5C3',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 24,
    marginLeft: 10,
    alignSelf: 'center',
  },
ImgLocation: {
    textAlign: "center",
    alignSelf: 'center',
    width: '100%',
    height: '100%'
  },
  headerRigh: {
    backgroundColor: "#00B5C3",
    width: 54,
    height: 54,
    borderRadius: 100,
    alignSelf: 'center',
  },
  input: {
    backgroundColor: '#fff',
    width: 324,
    height: 42,
    borderRadius: 17,
    alignSelf: 'center',
    marginTop: 15,
    paddingLeft:15
  },
 
});
