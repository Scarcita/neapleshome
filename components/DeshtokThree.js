import React from "react";
import { View,Text, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList } from "react-native";

////IMAGENES//////

import ImgPortada from '../assets/Img/meeting.svg'
const DeshtokThree = ({navigation}) => {


  return ( 
        <View>
          <ImgPortada
              style={styles.imgPeople}
            />

          <Text style={styles.title}>
          Influence and connections to boost your business
          </Text>

          <Text style={styles.description}>
          Get in touch with people with your same aspirations and willing to make a change in Naples
          </Text>

          <TouchableOpacity
            onPress={() => navigation.navigate("DeshtokTwo")}
              style={styles.button}
          >
            <Text
                style={styles.textButton}
            >
                Continue
            </Text>
          </TouchableOpacity>
        </View>
      
    
  )
    
}

export default DeshtokThree;

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    backgroundColor: '#000000',
    
    
  },
  imgPeople: {
    textAlign: "center",
    alignSelf: 'center',
    width: '100%',
    height: '100%'
  },
  title: {
    color: '#000000',
    fontWeight: 'bold',
    fontSize: 35,
    marginTop: 10,
    textAlign: "center"
  },

  description: {
    color: '#000000',
    //fontWeight: 'bold',
    fontSize: 14,
    marginTop: 10,
    textAlign: "center"
  },
  button: {
    backgroundColor: "#FDB827",
    marginTop: 20,
    width: 320,
    height: 58,
    alignSelf: 'center',
    borderRadius: 17,

  },
  textButton: {
    textAlign: 'center',
    fontSize: 24,
    color: '#FFFFFF',
    paddingTop: 13,
    //fontFamily: 'Prompt_600SemiBold',
  }

});
