import React from "react";
import { View,Text, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList, TextInput } from "react-native";

//// IMAGENES////////

const ListEvents = ({navigation}) => {


  return ( 
      <View  style={[styles.flex, {
            flexDirection: "row", marginTop: 20
          }]}>
        <View style={{ flex: 1, backgroundColor: 'red', height: 80}} >
          <Text>hola</Text>
          
        </View>
        <View style={{ flex: 1, backgroundColor: 'blue', height: 80}} >
          <View style={styles.buttonNotificacion}>
            <Text style={styles.textNotification}>Business After 5</Text>
          </View>
        </View>
        <View style={{ flex: 1, backgroundColor: 'red', height: 80}} >
            
        </View>
    </View>

  )
}

export default ListEvents;

const styles = StyleSheet.create ({
  flex: {
    flex: 1,
  },
  buttonNotificacion: {
    width: 78,
    height: 14,
    backgroundColor: '#FDB827',
    borderRadius: 7
  },
  textNotification:{
    textAlign: 'center',
    color: '#fff',
    alignSelf: 'center'
  },
})