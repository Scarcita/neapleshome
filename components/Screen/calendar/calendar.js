import React, {useState, useEffect} from "react";
import { View,Text, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList, TextInput } from "react-native";
import ListEvents from "../../Screen/calendar/listaEvents";

//// IMAGENES////////

import ImgEvent from '../../../assets/Calendar/postEventSave.svg'
import ImgButtonSave from '../../../assets/Calendar/buttonSave.svg'

const CalendarScreen = ({navigation}) => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData]= useState([]);

  const getUsuarios = async () => {
    try{
      const response = await
      fetch("https://reqres.in/api/users?page=2");
      const json = await response.json();
      console.log(json);
      setData(json.data);
    } catch (error){
      console.error(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getUsuarios();
  }, []);


  return ( 
    <View style={styles.container}>
      <View>
        <Text style={styles.title}>Events calendar</Text>
      </View>
      <View>
        <TextInput 
        style={styles.input}
        placeholder='Search event'
        ></TextInput>
      </View>
      <View  style={[styles.flex, {
            flexDirection: "row", marginTop: 20
          }]}>
        <View style={{ flex: 1, height: 80}} >
          <View style={styles.backgroundDate}>
            <View style={styles.dateAll}>
              <Text style={styles.nameDay}>Mon</Text>
              <Text style={styles.dayDate}>17</Text>
              <Text style={styles.nameMonth}>Dec</Text>
            </View>
          </View> 
        </View>
        <View style={{ flex: 1, height: 80}} >
          <View style={styles.backgroundDate}>
            <View style={styles.dateAll}>
              <Text style={styles.nameDay}>Mon</Text>
              <Text style={styles.dayDate}>18</Text>
              <Text style={styles.nameMonth}>Dec</Text>
            </View>
          </View>  
        </View>
        <View style={{ flex: 1, height: 80}} >
          <View style={styles.backgroundToday}>
            <View style={styles.dateAll}>
              <Text style={styles.nameDayToday}>Mon</Text>
              <Text style={styles.dayDateToday}>19</Text>
              <Text style={styles.nameMonthToday}>Dec</Text>
            </View>
          </View>  
        </View>
        <View style={{ flex: 1, height: 80,}} >
          <View style={styles.backgroundDate}>
            <View style={styles.dateAll}>
              <Text style={styles.nameDay}>Mon</Text>
              <Text style={styles.dayDate}>20</Text>
              <Text style={styles.nameMonth}>Dec</Text>
            </View>
          </View> 
        </View>
        <View style={{ flex: 1, height: 80}} >
          <View style={styles.backgroundDate}>
            <View style={styles.dateAll}>
              <Text style={styles.nameDay}>Mon</Text>
              <Text style={styles.dayDate}>21</Text>
              <Text style={styles.nameMonth}>Dec</Text>
            </View>
          </View> 
      </View>
    </View>
    <FlatList
      //horizontal = {true}
        data = {data}
        keyExtractor= {({id}) => id}
        renderItem={({item})=> (
          <View  style={[styles.flex1, {
            flexDirection: "row", marginTop: 20
            }]}>
              <View style={{ flex: 2,}} >
                <ImgEvent style={{borderRadius: 17}}/>
              </View>
              <View style={{ flex: 3, alignSelf: 'center',}} >
                <View style={styles.buttonNotificacion}>
                  <Text style={styles.textNotification}>Business After 5</Text>
                </View>
                <Text style={styles.name}>{item.email}</Text>
                <Text style={styles.timeEvent}>10:00 AM to 05:00 PM</Text>
                <Text style={styles.nameLocation}>Rookery Bay</Text>
              </View>
              <View style={{ flex: 1, alignSelf: 'center', alignItems: 'center'}} >
                <ImgButtonSave/>   
              </View>
          </View>
        )} >       
    </FlatList>
   

    </View>

  )
}

export default CalendarScreen;

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    marginTop: 30,
    marginLeft: 35,
    marginRight:35,
    //backgroundColor: 'red'
  },
  title:{
    color: '#000000',
    fontSize: 17,
    fontWeight: '700',
    marginTop: 30
  },
  input: {
    backgroundColor: '#fff',
    width: 324,
    height: 42,
    borderRadius: 17,
    alignSelf: 'center',
    marginTop: 15,
    paddingLeft:15
  },
  flex: {
    flex: 1,
  },
  backgroundDate:{
    width: 60,
    height:72,
    backgroundColor: '#fff',
    borderRadius: 9,
    alignSelf: 'center'
  },
  dateAll:{
    textAlign: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    padding: 6
  },
  nameDay: {
    color: '#FF0000',
    fontWeight: '400',
    fontSize: 12,
  },
  dayDate: {
    color: '#000',
    fontWeight: '700',
    fontSize: 24,
  },
  nameMonth: {
    color: '#000',
    fontWeight: '400',
    fontSize: 12,
  },
  backgroundToday:{
    width: 60,
    height:72,
    backgroundColor: '#00B5C3',
    borderRadius: 9,
    alignSelf: 'center'
  },
  dateAll:{
    textAlign: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    padding: 6
  },
  nameDayToday: {
    color: '#fff',
    fontWeight: '400',
    fontSize: 12,
  },
  dayDateToday: {
    color: '#fff',
    fontWeight: '700',
    fontSize: 24,
  },
  nameMonthToday: {
    color: '#fff',
    fontWeight: '400',
    fontSize: 12,
  },
  flex1: {
    flex: 1,
  },
  buttonNotificacion: {
    width: 78,
    height: 14,
    backgroundColor: '#FDB827',
    borderRadius: 7
  },
  textNotification:{
    fontSize: 8,
    color: '#fff',
    alignSelf: 'center',
    alignSelf: 'center', 
    alignItems: 'center'
  },
  name: {
    fontSize: 14,
    fontWeight: '700',
    color: '#000'
  },
  timeEvent: {
    fontSize: 13,
    fontWeight: '500',
    color: '#747474'

  },
  timeEvent: {
    fontSize: 13,
    fontWeight: '500',
    color: '#747474'

  }
})